﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotController
{
    public struct MyQuat
    {

        public float w;
        public float x;
        public float y;
        public float z;


        public MyQuat(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
    }

    public struct MyVec
    {

        public float x;
        public float y;
        public float z;

        public MyVec(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public class MyRobotController
    {
        const float SLEERPSUM = 0.007f;

        #region public methods

        public void PutRobotStraight(out MyQuat rot0, out MyQuat rot1, out MyQuat rot2, out MyQuat rot3)
        {

            sleerpI = 0;

            sleerpII = 0;

            MyQuat tmp = new MyQuat(0, 0, 0, 1);
            MyVec tmpVec = new MyVec(1, 0, 0);

            rot0 = Rotate(tmp, new MyVec(0, 1, 0), 74);
            rot1 = Multiply(rot0, Rotate(tmp, tmpVec, 1));
            rot2 = Multiply(rot1, Rotate(tmp, tmpVec, 60));
            rot3 = Multiply(rot2, Rotate(tmp, tmpVec, 45));

            initEx2[0] = rot0;
            initEx2[1] = rot1;
            initEx2[2] = rot2;
            initEx2[3] = rot3;
        }

        public bool PickStudAnim(out MyQuat rot0, out MyQuat rot1, out MyQuat rot2, out MyQuat rot3)
        {
            endEx2[0] = NullQ;
            sleerpI += SLEERPSUM;

            bool slerpMoreThanOne = (sleerpI < 1) ? true : false;

            if (slerpMoreThanOne)
            {
                MyQuat tmp = new MyQuat(0, 0, 0, 1);
                MyVec tmpVec = new MyVec(1, 0, 0);

                rot0 = Rotate(tmp, new MyVec(0, 1, 0), 40);
                rot1 = Multiply(rot0, Rotate(tmp, tmpVec, 12));
                rot2 = Multiply(rot1, Rotate(tmp, tmpVec, 60));
                rot3 = Multiply(rot2, Rotate(tmp, tmpVec, 20));

                endEx2[0] = rot0;
                endEx2[1] = rot1;
                endEx2[2] = rot2;
                endEx2[3] = rot3;

                rot0 = slerp(initEx2[0], endEx2[0], sleerpI);
                rot1 = slerp(initEx2[1], endEx2[1], sleerpI);
                rot2 = slerp(initEx2[2], endEx2[2], sleerpI);
                rot3 = slerp(initEx2[3], endEx2[3], sleerpI);

                return true;
            }

            //todo: remove this once your code works.
            rot0 = endEx2[0];
            rot1 = endEx2[1];
            rot2 = endEx2[2];
            rot3 = endEx2[3];

            return false;
        }

        public bool PickStudAnimVertical(out MyQuat rot0, out MyQuat rot1, out MyQuat rot2, out MyQuat rot3)
        {
            endEx2[0] = NullQ;
            sleerpII += SLEERPSUM;

            bool slerpMoreThanOne = (sleerpII < 1) ? true : false;

            while (slerpMoreThanOne)
            {
                //todo: add your code here

                MyQuat tmp = new MyQuat(0, 0, 0, 1);
                MyVec tmpVec = new MyVec(1, 0, 0);

                rot0 = Rotate(tmp, new MyVec(0, 1, 0), 40);
                rot1 = Multiply(rot0, Rotate(tmp, tmpVec, 12));
                rot2 = Multiply(rot1, Rotate(tmp, tmpVec, 60));
                rot3 = Multiply(rot2, Rotate(tmp, tmpVec, 20));

                endEx2[0] = rot0;
                endEx2[1] = rot1;
                endEx2[2] = rot2;
                endEx3 = rot3;

                rot0 = slerp(initEx2[0], endEx2[0], sleerpII);
                rot1 = slerp(initEx2[1], endEx2[1], sleerpII);
                rot2 = slerp(initEx2[2], endEx2[2], sleerpII);
                rot3 = slerp(initEx2[3], endEx3, sleerpII);

                return true;

            }

            //todo: remove this once your code works.
            rot0 = endEx2[0];
            rot1 = endEx2[1];
            rot2 = endEx2[2];
            rot3 = endEx3;

            return false;
        }


        public static MyQuat GetSwing(MyQuat rot3) => rot3;

        public static MyQuat GetTwist(MyQuat rot3) => rot3 = new MyQuat(rot3.x / (float)(Math.Sqrt(Math.Pow(rot3.w, 2) + Math.Pow(rot3.x, 2))), rot3.y, rot3.z, rot3.w / (float)(Math.Sqrt(Math.Pow(rot3.w, 2) + Math.Pow(rot3.x, 2))));

        #endregion


        #region private and internal methods

        //INTS
        internal int TimeSinceMidnight { get { return (DateTime.Now.Hour * 3600000) + (DateTime.Now.Minute * 60000) + (DateTime.Now.Second * 1000) + DateTime.Now.Millisecond; } }
        

        //FLOATS

        //LengthSquared
        internal float GetLS(MyQuat q1)
            => (float)((double)q1.x * (double)q1.x + (double)q1.y * (double)q1.y + (double)q1.z * (double)q1.z + (double)q1.w * (double)q1.w);

        internal float Dot(MyQuat x, MyQuat y)
            => (float)((double)x.x * (double)y.x + (double)x.y * (double)y.y + (double)x.z * (double)y.z);

        internal float Dot(MyVec x, MyVec y)
            => (float)((double)x.x * (double)y.x + (double)x.y * (double)y.y + (double)x.z * (double)y.z);

        internal float Dot(float x1, float y1, float z1, float x2, float y2, float z2)
            => (float)((double)x1 * (double)x2 + (double)y1 * (double)y2 + (double)z1 * (double)z2);

        internal float sleerpI;
        internal float sleerpII;

        //MyQuats
        MyQuat[] initEx2 = new MyQuat[4];
        MyQuat[] endEx2 = new MyQuat[4];
        MyQuat endEx3;

        private static MyQuat NullQ => new MyQuat(1, 0, 0, 0);

        private static MyQuat ZeroQ => new MyQuat(0, 0, 0, 0);


        internal MyQuat Multiply(MyQuat q1, MyQuat q2)
        {

            MyQuat mQ = new MyQuat();

            mQ.w = (float)((double)q2.w * (double)q1.w - (double)q2.x * (double)q1.x - (double)q2.y * (double)q1.y - (double)q2.z * (double)q1.z);
            mQ.x = (float)((double)q2.w * (double)q1.x + (double)q2.x * (double)q1.w - (double)q2.y * (double)q1.z + (double)q2.z * (double)q1.y);
            mQ.y = (float)((double)q2.w * (double)q1.y + (double)q2.x * (double)q1.z + (double)q2.y * (double)q1.w - (double)q2.z * (double)q1.x);
            mQ.z = (float)((double)q2.w * (double)q1.z - (double)q2.x * (double)q1.y + (double)q2.y * (double)q1.x + (double)q2.z * (double)q1.w);

            mQ = Normalize(mQ);
            return mQ;

        }

        internal MyQuat Rotate(MyQuat currentRotation, MyVec axis, float angle)
        {
            MyQuat mQ = new MyQuat();
            float num = (float)Math.Sqrt(
                  Math.Pow((double)axis.x, 2.0) 
                + Math.Pow((double)axis.y, 2.0) 
                + Math.Pow((double)axis.z, 2.0));

            axis = new MyVec(axis.x / num, axis.y / num, axis.z / num);

            angle *= (float)(Math.PI / 180);

            mQ.x = axis.x * (float)Math.Sin((double)angle / 2.0);
            mQ.y = axis.y * (float)Math.Sin((double)angle / 2.0);
            mQ.z = axis.z * (float)Math.Sin((double)angle / 2.0);
            mQ.w = (float)Math.Cos((double)angle / 2.0);
            mQ = Normalize(mQ);

            currentRotation = Normalize(currentRotation);

            return Multiply(mQ, currentRotation);
        }

        internal MyQuat Normalize(MyQuat mQ)
        {
            double num = Math.Sqrt((double)mQ.x * (double)mQ.x + (double)mQ.y * (double)mQ.y + (double)mQ.z * (double)mQ.z + (double)mQ.w * (double)mQ.w);
            mQ.x /= (float)num;
            mQ.y /= (float)num;
            mQ.z /= (float)num;
            mQ.w /= (float)num;

            return mQ;
        }

        internal MyQuat slerp(MyQuat q1, MyQuat q2, float t)
        {
            MyQuat mQ = ZeroQ;

            if ((double)GetLS(q1) == 0.0)
            {
                if ((double)GetLS(q2) == 0.0)
                    return NullQ;
                return q2;
            }
            if ((double)GetLS(q2) == 0.0)
                return q1;

            float num1 = q1.w * q2.w + Dot(q1, q2);

            if ((double)num1 >= 1.0 || (double)num1 <= -1.0)
                return q1;
            if ((double)num1 < 0.0)
            {
                num1 *= -1;
                q2.x *= -1;
                q2.y *= -1;
                q2.z *= -1;
            }
            float num2;
            float num3;
            if ((double)num1 < 0.99)
            {
                float acosn1 = (float)Math.Acos((double)num1);
                float sinacosn1 = 1f / (float)Math.Sin((double)acosn1);
                num2 = (float)Math.Sin((double)acosn1 * (1.0 - (double)t)) * sinacosn1;
                num3 = (float)Math.Sin((double)acosn1 * (double)t) * sinacosn1;
            }
            else
            {
                num2 = 1f - t;
                num3 = t;
            }
            float num4 = num2 * q1.x;
            float num5 = num2 * q1.y;
            float num6 = num2 * q1.z;
            float num7 = num3 * q2.x;
            float num8 = num3 * q2.y;
            float num9 = num3 * q2.z;
            float num10 = num2 * q1.w;
            float num11 = num3 * q2.w;

            MyQuat q1_1 = new MyQuat();
            q1_1.w = num10 + num11;
            q1_1.x = num4 + num7;
            q1_1.y = num5 + num8;
            q1_1.z = num6 + num9;

            if ((double)GetLS(q1_1) <= 0.0)
                return NullQ;

            q1_1 = Normalize(q1_1);
            return q1_1;
        }
    }
}

#endregion